package covidsecure.uk.venuecheckin;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import covidsecure.uk.venuecheckin.database.AppDatabase;
import covidsecure.uk.venuecheckin.database.CheckIn;

public class HistoryActivity extends AppCompatActivity
{
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.history_activity);
		setSupportActionBar(findViewById(R.id.topAppBar));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		HistoryAdapter adapter = new HistoryAdapter();
		RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
		RecyclerView historyList = findViewById(R.id.historyList);
		historyList.setAdapter(adapter);
		historyList.setLayoutManager(layoutManager);
		ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(adapter));
		itemTouchHelper.attachToRecyclerView(historyList);

		((RecyclerView) findViewById(R.id.historyList)).setNestedScrollingEnabled(false);

		AppDatabase.getInstance(getApplicationContext()).checkInDao().getCheckInHistory().observe(this, new Observer<CheckIn[]>()
		{
			@Override
			public void onChanged(CheckIn[] checkIns)
			{
				if (checkIns == null)
				{
					findViewById(R.id.historyList).setVisibility(View.GONE);
					findViewById(R.id.historyEmpty).setVisibility(View.GONE);
				}
				else if (checkIns.length == 0)
				{
					findViewById(R.id.historyList).setVisibility(View.GONE);
					findViewById(R.id.historyEmpty).setVisibility(View.VISIBLE);
				}
				else
				{
					findViewById(R.id.historyList).setVisibility(View.VISIBLE);
					findViewById(R.id.historyEmpty).setVisibility(View.GONE);

					((HistoryAdapter) ((RecyclerView) findViewById(R.id.historyList)).getAdapter()).submitList(Arrays.asList(checkIns));
				}
			}
		});
	}

	public static class HistoryAdapter extends ListAdapter<CheckIn, HistoryAdapter.HistoryViewHolder>
	{
		public static class HistoryViewHolder extends RecyclerView.ViewHolder
		{
			public HistoryViewHolder(View v)
			{
				super(v);
			}

			public void bindTo(CheckIn item)
			{
				Context context = itemView.getContext();

				((TextView) itemView.findViewById(R.id.venue)).setText(item.getVenueName());

				String date;
				LocalDateTime start = LocalDateTime.ofInstant(Instant.ofEpochSecond(item.getStart()), ZoneId.systemDefault());
				if (start.isAfter(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT)))
				{
					date = context.getString(R.string.history_item_time_today);
				}
				else if (start.isAfter(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).minusDays(1)))
				{
					date = context.getString(R.string.history_item_time_yesterday);
				}
				else
				{
					date = start.format(DateTimeFormatter.ofPattern("eee d"));
				}
				String time = start.format(DateTimeFormatter.ofPattern("HH:mm"));
				((TextView) itemView.findViewById(R.id.time)).setText(context.getString(R.string.history_item_time, date, time));
			}
		}

		public HistoryAdapter()
		{
			super(DIFF_CALLBACK);
		}

		@NonNull
		@Override
		public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
		{
			View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_item, parent, false);
			HistoryViewHolder vh = new HistoryViewHolder(v);
			return vh;
		}

		@Override
		public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position)
		{
			holder.bindTo(getItem(position));
		}

		public static final DiffUtil.ItemCallback<CheckIn> DIFF_CALLBACK = new DiffUtil.ItemCallback<CheckIn>()
		{
			@Override
			public boolean areItemsTheSame(@NonNull CheckIn oldCheckIn, @NonNull CheckIn newCheckIn)
			{
				return oldCheckIn.getVenueId().equals(newCheckIn.getVenueId()) && oldCheckIn.getStart() == newCheckIn.getStart() && oldCheckIn.getEnd() == newCheckIn.getEnd();
			}

			@Override
			public boolean areContentsTheSame(@NonNull CheckIn oldCheckIn, @NonNull CheckIn newCheckIn)
			{
				return oldCheckIn.getVenueId().equals(newCheckIn.getVenueId()) && oldCheckIn.getStart() == newCheckIn.getStart() && oldCheckIn.getEnd() == newCheckIn.getEnd();
			}
		};
	}

	private static class SwipeToDeleteCallback extends ItemTouchHelper.Callback
	{
		private final HistoryAdapter adapter;

		public SwipeToDeleteCallback(HistoryAdapter adapter)
		{
			this.adapter = adapter;
		}

		@Override
		public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder)
		{
			return makeMovementFlags(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
		}

		@Override
		public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target)
		{
			return false;
		}

		@Override
		public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction)
		{
			CheckIn item = adapter.getCurrentList().get(viewHolder.getAdapterPosition());
			new RemoveAsyncTask(viewHolder.itemView.getContext().getApplicationContext()).execute(item);
		}
	}

	private static class RemoveAsyncTask extends AsyncTask<CheckIn, Void, Void>
	{
		private final Context context;

		private RemoveAsyncTask(Context context)
		{
			this.context = context;
		}

		@Override
		protected Void doInBackground(CheckIn... checkIns)
		{
			AppDatabase.getInstance(context).checkInDao().removeCheckIn(checkIns[0]);
			return null;
		}
	}
}