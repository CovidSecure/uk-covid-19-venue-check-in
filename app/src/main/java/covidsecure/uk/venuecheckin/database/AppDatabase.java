package covidsecure.uk.venuecheckin.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {CheckIn.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase
{
	public abstract CheckInDao checkInDao();

	private static volatile AppDatabase instance;

	public synchronized static AppDatabase getInstance(Context context)
	{
		if (instance == null)
		{
			instance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "database").build();
		}
		return instance;
	}
}