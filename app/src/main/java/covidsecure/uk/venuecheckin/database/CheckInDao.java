package covidsecure.uk.venuecheckin.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import covidsecure.uk.venuecheckin.Venue;

@Dao
public abstract class CheckInDao
{
	public void checkIn(Venue venue, long now)
	{
		long end = ZonedDateTime.ofInstant(Instant.ofEpochSecond(now), ZoneId.systemDefault()).withHour(0).withMinute(0).withSecond(0).plusDays(1).minusSeconds(1).toEpochSecond();
		CheckIn checkIn = new CheckIn(0, venue.id, venue.name, now, end, false, false);
		insert(checkIn);
	}

	@Insert
	protected abstract void insert(CheckIn checkIn);

	@Query("UPDATE CheckIn SET `end` = :now, manualCheckOut = 1 WHERE `end` > :now AND NOT manualCheckOut")
	public abstract void checkOut(long now);

	@Query("SELECT * FROM CheckIn WHERE `end` > :now AND NOT manualCheckOut ORDER BY start DESC LIMIT 1")
	public abstract LiveData<CheckIn> getActiveCheckIn(long now);

	@Query("SELECT * FROM CheckIn ORDER BY start ASC")
	public abstract LiveData<CheckIn[]> getCheckInHistory();

	@Query("SELECT * FROM CheckIn ORDER BY start ASC")
	public abstract CheckIn[] getCheckInHistorySynchronously();

	@Delete
	public abstract void removeCheckIn(CheckIn checkIn);

	@Query("UPDATE CheckIn SET notified = 1")
	public abstract void markAllAsNotified();
}