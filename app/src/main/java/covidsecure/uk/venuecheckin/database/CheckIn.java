package covidsecure.uk.venuecheckin.database;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class CheckIn
{
	@PrimaryKey(autoGenerate = true)
	private final int id;
	private final String venueId;
	private final String venueName;
	private final long start;
	private final long end;
	private final boolean manualCheckOut;
	private final boolean notified;

	protected CheckIn(int id, String venueId, String venueName, long start, long end, boolean manualCheckOut, boolean notified)
	{
		this.id = id;
		this.venueId = venueId;
		this.venueName = venueName;
		this.start = start;
		this.end = end;
		this.manualCheckOut = manualCheckOut;
		this.notified = notified;
	}

	protected int getId()
	{
		return id;
	}

	public String getVenueId()
	{
		return venueId;
	}

	public String getVenueName()
	{
		return venueName;
	}

	public long getStart()
	{
		return start;
	}

	public long getEnd()
	{
		return end;
	}

	protected boolean getManualCheckOut()
	{
		return manualCheckOut;
	}

	public boolean getNotified()
	{
		return notified;
	}
}