package covidsecure.uk.venuecheckin;

import android.util.Base64;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class Venue implements Serializable
{
	public final String id;
	public final String name;

	private Venue(String id, String name)
	{
		this.id = id;
		this.name = name;
	}

	public static Venue fromCode(String string) throws ClientException, IncorrectCodeTypeException, UnsupportedVersionException, MalformedCodeException, InvalidSignatureException
	{
		String[] parts = string.split(":");

		if (parts.length < 2)
		{
			throw new IncorrectCodeTypeException();
		}

		if (!parts[0].equals("UKC19TRACING"))
		{
			throw new IncorrectCodeTypeException();
		}

		int version;
		try
		{
			version = Integer.parseInt(parts[1]);
		}
		catch (NumberFormatException exception)
		{
			throw new MalformedCodeException(exception);
		}

		switch (version)
		{
			case 1:
			{
				if (parts.length != 3)
				{
					throw new MalformedCodeException();
				}

				parts = parts[2].split("\\.");
				if (parts.length != 3)
				{
					throw new MalformedCodeException();
				}

				JsonElement json;
				try
				{
					json = JsonParser.parseString(new String(Base64.decode(parts[0], Base64.NO_PADDING | Base64.NO_WRAP | Base64.URL_SAFE)));
				}
				catch (JsonSyntaxException exception)
				{
					throw new MalformedCodeException(exception);
				}
				try
				{
					if (!json.getAsJsonObject().get("alg").getAsString().equals("ES256") || !json.getAsJsonObject().get("kid").getAsString().equals("YrqeLTq8z-ofH5nzlaSGnYRfB9bu9yPlWYU_2b6qXOQ"))
					{
						throw new UnsupportedVersionException();
					}
				}
				catch (NullPointerException | ClassCastException | IllegalStateException exception)
				{
					throw new MalformedCodeException(exception);
				}
				try
				{
					PublicKey key = KeyFactory.getInstance("EC").generatePublic(new X509EncodedKeySpec(Base64.decode("MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEVyPX5CqxSdg0JE/2O0wog0qmvza/GLVAxNmJROQwMjtzk1U5YESBvk7njod0LJwVTmw+w/aIRvvnQsB1w5CW1g==", Base64.NO_PADDING | Base64.NO_WRAP)));
					Signature signature = Signature.getInstance("SHA256withECDSA");
					signature.initVerify(key);
					signature.update((parts[0] + "." + parts[1]).getBytes());
					if (!signature.verify(convertRawSignature(Base64.decode(parts[2], Base64.NO_PADDING | Base64.NO_WRAP | Base64.URL_SAFE))))
					{
						throw new InvalidSignatureException();
					}
				}
				catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | SignatureException exception)
				{
					throw new ClientException(exception);
				}

				try
				{
					json = JsonParser.parseString(new String(Base64.decode(parts[1], Base64.NO_PADDING | Base64.NO_WRAP | Base64.URL_SAFE)));
				}
				catch (JsonSyntaxException exception)
				{
					throw new MalformedCodeException(exception);
				}
				try
				{
					String id = json.getAsJsonObject().get("id").getAsString();
					String name = json.getAsJsonObject().get("opn").getAsString();
					return new Venue(id, name);
				}
				catch (NullPointerException | ClassCastException | IllegalStateException exception)
				{
					throw new MalformedCodeException(exception);
				}
			}
			default:
				throw new UnsupportedVersionException();
		}
	}

	private static byte[] convertRawSignature(byte[] raw)
	{
		ByteBuffer buffer = ByteBuffer.allocate(72);

		buffer.put((byte) 0x30);
		// length to be determined later
		buffer.put((byte) 0x00);

		buffer.put((byte) 0x02);
		int raw_length = 32;
		while (raw[32 - raw_length] == 0x00)
		{
			raw_length--;
		}
		if ((raw[32 - raw_length] & 0x80) != 0x00)
		{
			buffer.put((byte) (raw_length + 1));
			buffer.put((byte) 0x00);
		}
		else
		{
			buffer.put((byte) raw_length);
		}
		buffer.put(raw, 32 - raw_length, raw_length);

		buffer.put((byte) 0x02);
		raw_length = 32;
		while (raw[64 - raw_length] == 0x00)
		{
			raw_length--;
		}
		if ((raw[64 - raw_length] & 0x80) != 0x00)
		{
			buffer.put((byte) (raw_length + 1));
			buffer.put((byte) 0x00);
		}
		else
		{
			buffer.put((byte) raw_length);
		}
		buffer.put(raw, 64 - raw_length, raw_length);

		int length = buffer.position();
		byte[] der = new byte[length];
		buffer.rewind();
		buffer.get(der);
		der[1] = (byte) (length - 2);

		return der;
	}

	public static class ClientException extends Exception
	{
		public ClientException()
		{
			// empty
		}

		public ClientException(Throwable cause)
		{
			super(cause);
		}
	}

	public static class IncorrectCodeTypeException extends Exception
	{
		// empty
	}

	public static class UnsupportedVersionException extends Exception
	{
		// empty
	}

	public static class MalformedCodeException extends Exception
	{
		public MalformedCodeException()
		{
			// empty
		}

		public MalformedCodeException(Throwable cause)
		{
			super(cause);
		}
	}

	public static class InvalidSignatureException extends Exception
	{
		// empty
	}
}