package covidsecure.uk.venuecheckin;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javax.net.ssl.HttpsURLConnection;

import covidsecure.uk.venuecheckin.database.AppDatabase;
import covidsecure.uk.venuecheckin.database.CheckIn;

public class RiskyVenuesWorker extends Worker
{
	private static final String RISKY_VENUES_URL = "https://distribution-te-prod.prod.svc-test-trace.nhs.uk/distribution/risky-venues";
	private static final String RISKY_VENUES_USER_AGENT = BuildConfig.APPLICATION_ID;
	public static final String RISKY_VENUES_LAST_UPDATED_PREFERENCE_KEY = "riskyVenuesLastUpdated";

	public RiskyVenuesWorker(@NonNull Context context, @NonNull WorkerParameters workerParams)
	{
		super(context, workerParams);
	}

	@NonNull
	@Override
	public Result doWork()
	{
		RiskyVenue[] riskyVenues;
		try
		{
			riskyVenues = downloadRiskyVenuesList();
		}
		catch (ConnectionException | MalformedListException exception)
		{
			return Result.success();
		}

		boolean notify = false;
		for (CheckIn checkIn : AppDatabase.getInstance(getApplicationContext()).checkInDao().getCheckInHistorySynchronously())
		{
			if (!checkIn.getNotified())
			{
				for (RiskyVenue venue : riskyVenues)
				{
					if (checkIn.getVenueId().equals(venue.id))
					{
						if (checkIn.getStart() <= venue.riskyWindow.until.toEpochSecond() && checkIn.getEnd() >= venue.riskyWindow.from.toEpochSecond())
						{
							notify = true;
							break;
						}
					}
				}
			}
		}

		if (notify)
		{
			AppDatabase.getInstance(getApplicationContext()).checkInDao().markAllAsNotified();
			Notifications.showRiskyVenueNotification(getApplicationContext());
		}

		PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putLong(RISKY_VENUES_LAST_UPDATED_PREFERENCE_KEY, Instant.now().getEpochSecond()).apply();

		return Result.success();
	}

	private RiskyVenue[] downloadRiskyVenuesList() throws ConnectionException, MalformedListException
	{
		JsonElement json;
		try
		{
			HttpsURLConnection connection = (HttpsURLConnection) new URL(RISKY_VENUES_URL).openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent", RISKY_VENUES_USER_AGENT);
			connection.setDoOutput(false);
			connection.setDoInput(true);
			try (InputStream stream = connection.getInputStream())
			{
				json = JsonParser.parseReader(new InputStreamReader(stream));
			}
		}
		catch (IOException exception)
		{
			throw new ConnectionException(exception);
		}
		catch (JsonIOException exception)
		{
			throw new ConnectionException(exception);
		}
		catch (JsonSyntaxException exception)
		{
			throw new MalformedListException(exception);
		}

		JsonArray venues;
		try
		{
			venues = json.getAsJsonObject().get("venues").getAsJsonArray();
		}
		catch (NullPointerException | ClassCastException | IllegalStateException exception)
		{
			throw new MalformedListException(exception);
		}

		RiskyVenue[] venues1;
		Gson gson = new GsonBuilder().registerTypeAdapter(ZonedDateTime.class, new ZonedDateTimeAdapter()).create();
		try
		{
			venues1 = gson.fromJson(venues, RiskyVenue[].class);
		}
		catch (JsonSyntaxException exception)
		{
			throw new MalformedListException(exception);
		}
		if (venues1 == null)
		{
			throw new MalformedListException();
		}
		for (RiskyVenue venue : venues1)
		{
			if (venue.id == null || venue.riskyWindow == null || venue.riskyWindow.from == null || venue.riskyWindow.until == null)
			{
				throw new MalformedListException();
			}
		}
		return venues1;
	}

	private static class RiskyVenue
	{
		private String id;
		private RiskyWindow riskyWindow;

		private static class RiskyWindow
		{
			private ZonedDateTime from;
			private ZonedDateTime until;
		}
	}

	private static class ZonedDateTimeAdapter implements JsonDeserializer<ZonedDateTime>
	{
		@Override
		public ZonedDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
		{
			try
			{
				return ZonedDateTime.parse(json.getAsString(), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
			}
			catch (Exception exception)
			{
				throw new JsonSyntaxException(exception);
			}
		}
	}

	private static class ConnectionException extends Exception
	{
		public ConnectionException()
		{
			// empty
		}

		public ConnectionException(Throwable cause)
		{
			super(cause);
		}
	}

	private static class MalformedListException extends Exception
	{
		public MalformedListException()
		{
			// empty
		}

		public MalformedListException(Throwable cause)
		{
			super(cause);
		}
	}
}