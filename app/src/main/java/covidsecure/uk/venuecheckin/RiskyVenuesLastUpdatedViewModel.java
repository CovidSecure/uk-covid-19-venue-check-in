package covidsecure.uk.venuecheckin;

import android.app.Application;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.preference.PreferenceManager;

public class RiskyVenuesLastUpdatedViewModel extends AndroidViewModel
{
	private final RiskyVenuesLastUpdatedLiveData riskyVenuesLastUpdated;

	public RiskyVenuesLastUpdatedViewModel(@NonNull Application application)
	{
		super(application);
		riskyVenuesLastUpdated = new RiskyVenuesLastUpdatedLiveData(PreferenceManager.getDefaultSharedPreferences(getApplication()));
	}

	public LiveData<Long> getRiskyVenuesLastUpdated()
	{
		return riskyVenuesLastUpdated;
	}

	private static class RiskyVenuesLastUpdatedLiveData extends LiveData<Long> implements SharedPreferences.OnSharedPreferenceChangeListener
	{
		private final SharedPreferences preferences;

		public RiskyVenuesLastUpdatedLiveData(SharedPreferences preferences)
		{
			this.preferences = preferences;
		}

		@Override
		protected void onActive()
		{
			preferences.registerOnSharedPreferenceChangeListener(this);
			new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					postValue(preferences.getLong(RiskyVenuesWorker.RISKY_VENUES_LAST_UPDATED_PREFERENCE_KEY, 0));
				}
			}).start();
		}

		@Override
		protected void onInactive()
		{
			preferences.unregisterOnSharedPreferenceChangeListener(this);
		}

		@Override
		public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
		{
			if (key.equals(RiskyVenuesWorker.RISKY_VENUES_LAST_UPDATED_PREFERENCE_KEY))
			{
				postValue(sharedPreferences.getLong(RiskyVenuesWorker.RISKY_VENUES_LAST_UPDATED_PREFERENCE_KEY, 0));
			}
		}
	}
}