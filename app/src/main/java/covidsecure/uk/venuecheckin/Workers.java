package covidsecure.uk.venuecheckin;

import android.content.Context;

import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import java.util.concurrent.TimeUnit;

public class Workers
{
	private static final String CLEAR_OLD_HISTORY_WORK_NAME = "clearOldData";
	private static final String STALE_DATA_NOTIFICATION_WORK_NAME = "staleDataNotification";
	private static final String RISKY_VENUES_WORK_NAME = "riskyVenues";

	public static void scheduleWork(Context context)
	{
		Constraints networkConstraint = new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build();
		PeriodicWorkRequest clearOldHistory = new PeriodicWorkRequest.Builder(ClearOldHistoryWorker.class, 3, TimeUnit.HOURS).build();
		PeriodicWorkRequest staleDataNotification = new PeriodicWorkRequest.Builder(StaleDataNotificationWorker.class, 3, TimeUnit.HOURS).build();
		PeriodicWorkRequest riskyVenues = new PeriodicWorkRequest.Builder(RiskyVenuesWorker.class, 2, TimeUnit.HOURS).setConstraints(networkConstraint).build();
		WorkManager manager = WorkManager.getInstance(context);
		manager.enqueueUniquePeriodicWork(CLEAR_OLD_HISTORY_WORK_NAME, ExistingPeriodicWorkPolicy.KEEP, clearOldHistory);
		manager.enqueueUniquePeriodicWork(STALE_DATA_NOTIFICATION_WORK_NAME, ExistingPeriodicWorkPolicy.KEEP, staleDataNotification);
		manager.enqueueUniquePeriodicWork(RISKY_VENUES_WORK_NAME, ExistingPeriodicWorkPolicy.KEEP, riskyVenues);
	}
}