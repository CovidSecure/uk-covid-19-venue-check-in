package covidsecure.uk.venuecheckin;

import android.animation.TimeAnimator;
import android.app.Application;

import androidx.annotation.NonNull;
import androidx.arch.core.util.Function;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.time.Instant;

import covidsecure.uk.venuecheckin.database.AppDatabase;
import covidsecure.uk.venuecheckin.database.CheckIn;

public class ActiveCheckInViewModel extends AndroidViewModel
{
	private MutableLiveData<Instant> currentTime;
	private TimeAnimator timeAnimator;
	private LiveData<CheckIn> activeCheckIn;

	public ActiveCheckInViewModel(@NonNull Application application)
	{
		super(application);
		currentTime = new MutableLiveData<>(Instant.now());
		timeAnimator = new TimeAnimator();
		timeAnimator.setTimeListener(new TimeAnimator.TimeListener()
		{
			private long delta = 0;

			@Override
			public void onTimeUpdate(TimeAnimator animation, long totalTime, long deltaTime)
			{
				delta += deltaTime;
				if (delta > 1000)
				{
					delta -= 1000;
					currentTime.postValue(Instant.now());
				}
			}
		});
		timeAnimator.start();
	}

	@Override
	protected void onCleared()
	{
		timeAnimator.cancel();
	}

	public LiveData<CheckIn> getActiveCheckIn()
	{
		if (activeCheckIn == null)
		{
			activeCheckIn = Transformations.switchMap(currentTime, new Function<Instant, LiveData<CheckIn>>()
			{
				@Override
				public LiveData<CheckIn> apply(Instant input)
				{
					return AppDatabase.getInstance(getApplication()).checkInDao().getActiveCheckIn(input.getEpochSecond());
				}
			});
		}
		return activeCheckIn;
	}
}