package covidsecure.uk.venuecheckin;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import covidsecure.uk.venuecheckin.database.AppDatabase;
import covidsecure.uk.venuecheckin.database.CheckIn;

public class StatusActivity extends AppCompatActivity
{
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.status_activity);
		setSupportActionBar(findViewById(R.id.topAppBar));

		ViewModelProvider viewModelProvider = new ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(getApplication()));
		viewModelProvider.get(ActiveCheckInViewModel.class).getActiveCheckIn().observe(this, new Observer<CheckIn>()
		{
			@Override
			public void onChanged(CheckIn checkIn)
			{
				if (checkIn != null)
				{
					showCheckIn(checkIn);
				}
				else
				{
					showNoCheckIn();
				}
			}
		});
		viewModelProvider.get(RiskyVenuesLastUpdatedViewModel.class).getRiskyVenuesLastUpdated().observe(this, new Observer<Long>()
		{
			@Override
			public void onChanged(Long aLong)
			{
				if (aLong != 0)
				{
					String last_updated = LocalDateTime.ofInstant(Instant.ofEpochSecond(aLong), ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
					((TextView) findViewById(R.id.last_updated)).setText(getString(R.string.last_updated, last_updated));
				}
				else
				{
					((TextView) findViewById(R.id.last_updated)).setText(getString(R.string.last_updated, getString(R.string.last_updated_never)));
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.status_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item)
	{
		switch (item.getItemId())
		{
			case R.id.history:
				Intent intent = new Intent(this, HistoryActivity.class);
				startActivity(intent);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK && data != null)
		{
			new CheckInAsyncTask(getApplicationContext()).execute((Venue) data.getSerializableExtra(ScanActivity.EXTRA_VENUE));
		}
	}

	private void showCheckIn(CheckIn checkIn)
	{
		((TextView) findViewById(R.id.message)).setText(R.string.status_checked_in_at);
		((TextView) findViewById(R.id.venue)).setText(checkIn.getVenueName());
		((TextView) findViewById(R.id.start)).setText(getString(R.string.status_checked_in_since, LocalDateTime.ofInstant(Instant.ofEpochSecond(checkIn.getStart()), ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern("HH:mm"))));
		findViewById(R.id.message).setVisibility(View.VISIBLE);
		findViewById(R.id.venue).setVisibility(View.VISIBLE);
		findViewById(R.id.start).setVisibility(View.VISIBLE);

		((Button) findViewById(R.id.action)).setText(R.string.check_out);
		findViewById(R.id.action).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				new CheckOutAsyncTask(getApplicationContext()).execute();
			}
		});
		findViewById(R.id.action).setVisibility(View.VISIBLE);
	}

	private void showNoCheckIn()
	{
		((TextView) findViewById(R.id.message)).setText(R.string.status_not_checked_in);
		findViewById(R.id.message).setVisibility(View.VISIBLE);
		findViewById(R.id.venue).setVisibility(View.GONE);
		findViewById(R.id.start).setVisibility(View.GONE);

		((Button) findViewById(R.id.action)).setText(R.string.check_in);
		findViewById(R.id.action).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				Intent intent = new Intent(StatusActivity.this, ScanActivity.class);
				startActivityForResult(intent, 0);
			}
		});
		findViewById(R.id.action).setVisibility(View.VISIBLE);
	}

	private static class CheckInAsyncTask extends AsyncTask<Venue, Void, Void>
	{
		private final Context context;

		private CheckInAsyncTask(Context context)
		{
			this.context = context;
		}

		@Override
		protected Void doInBackground(Venue... venues)
		{
			AppDatabase.getInstance(context).checkInDao().checkIn(venues[0], Instant.now().getEpochSecond());
			return null;
		}
	}

	private static class CheckOutAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private final Context context;

		private CheckOutAsyncTask(Context context)
		{
			this.context = context;
		}

		@Override
		protected Void doInBackground(Void... voids)
		{
			AppDatabase.getInstance(context).checkInDao().checkOut(Instant.now().getEpochSecond());
			return null;
		}
	}
}