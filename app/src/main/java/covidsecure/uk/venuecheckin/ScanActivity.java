package covidsecure.uk.venuecheckin;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.Animatable;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.Semaphore;

public class ScanActivity extends AppCompatActivity
{
	public static final String EXTRA_VENUE = "venue";

	private static final int CAMERA_PERMISSION_REQUEST_CODE = 1;

	private Camera mCamera;
	private int mCameraRotation;
	private boolean mCameraMirrored;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.scan_activity);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		((TextureView) findViewById(R.id.preview)).setSurfaceTextureListener(new TextureView.SurfaceTextureListener()
		{
			@Override
			public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height)
			{
				if (mCamera != null)
				{
					try
					{
						mCamera.setPreviewTexture(surface);
						setPreviewTransformation();
					}
					catch (IOException exception)
					{
						// empty
					}
				}
			}

			@Override
			public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height)
			{
				setPreviewTransformation();
			}

			@Override
			public boolean onSurfaceTextureDestroyed(SurfaceTexture surface)
			{
				if (mCamera != null)
				{
					mCamera.stopPreview();
					try
					{
						mCamera.setPreviewTexture(null);
					}
					catch (IOException exception)
					{
						// empty
					}
					mCamera.startPreview();
				}
				return true;
			}

			@Override
			public void onSurfaceTextureUpdated(SurfaceTexture surface)
			{
				// empty
			}
		});
		((TextureView) findViewById(R.id.preview)).setOpaque(false);

		findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				setResult(RESULT_CANCELED);
				finish();
			}
		});

		if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
		{
			findViewById(R.id.permission_warning).setVisibility(View.VISIBLE);
			findViewById(R.id.cancel).setVisibility(View.VISIBLE);
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
		}
	}

	@Override
	protected void onStart()
	{
		super.onStart();

		if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
		{
			startCamera();
		}
	}

	@Override
	protected void onStop()
	{
		super.onStop();

		if (mCamera != null)
		{
			mCamera.stopPreview();
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
	{
		if (requestCode == CAMERA_PERMISSION_REQUEST_CODE)
		{
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
			{
				findViewById(R.id.permission_warning).setVisibility(View.GONE);
				startCamera();
			}
		}
	}

	private void startCamera()
	{
		findViewById(R.id.icon).setVisibility(View.GONE);
		findViewById(R.id.bottom).setVisibility(View.GONE);
		findViewById(R.id.cancel).setVisibility(View.VISIBLE);

		// open camera

		int id;
		Camera.CameraInfo info = new Camera.CameraInfo();
		for (id = 0; id < Camera.getNumberOfCameras(); id++)
		{
			Camera.getCameraInfo(id, info);
			if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
			{
				break;
			}
		}
		if (id == Camera.getNumberOfCameras())
		{
			id = 0;
			Camera.getCameraInfo(id, info);
		}
		mCamera = Camera.open(id);

		// determine camera rotation

		int display_rotation = getWindowManager().getDefaultDisplay().getRotation();
		int display_rotation_degrees = 0;
		switch (display_rotation)
		{
			case Surface.ROTATION_0:
				display_rotation_degrees = 0;
				break;
			case Surface.ROTATION_90:
				display_rotation_degrees = 90;
				break;
			case Surface.ROTATION_180:
				display_rotation_degrees = 180;
				break;
			case Surface.ROTATION_270:
				display_rotation_degrees = 270;
				break;
		}
		mCameraRotation = info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT ? ((360 - ((info.orientation + display_rotation) % 360)) % 360) : ((info.orientation - display_rotation_degrees + 360) % 360);
		mCameraMirrored = info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT;
		mCamera.setDisplayOrientation(mCameraRotation);

		// set camera parameters

		Camera.Parameters parameters = mCamera.getParameters();

		if (parameters.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
		{
			parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
		}
		if (parameters.getSupportedSceneModes() != null && parameters.getSupportedSceneModes().contains(Camera.Parameters.SCENE_MODE_BARCODE))
		{
			parameters.setSceneMode(Camera.Parameters.SCENE_MODE_BARCODE);
		}

		int max_pixels = 0;
		Camera.Size max_size = null;
		for (Camera.Size size : parameters.getSupportedPreviewSizes())
		{
			int pixels = size.width * size.height;
			if (pixels > max_pixels)
			{
				max_pixels = pixels;
				max_size = size;
			}
		}
		parameters.setPreviewSize(max_size.width, max_size.height);

		mCamera.setParameters(parameters);

		// preview callback

		final int preview_width = max_size.width;
		final int preview_height = max_size.height;
		mCamera.setPreviewCallback(new Camera.PreviewCallback()
		{
			private Semaphore lock = new Semaphore(1, true);
			private boolean haveCode = false;

			@Override
			public void onPreviewFrame(byte[] data, Camera camera)
			{
				if (haveCode)
				{
					return;
				}

				final byte[] copy = Arrays.copyOf(data, data.length);
				if (lock.tryAcquire())
				{
					new Thread(new Runnable()
					{
						@Override
						public void run()
						{
							try
							{
								// image -> QR

								Result result;
								try
								{
									result = new QRCodeReader().decode(new BinaryBitmap(new HybridBinarizer(new PlanarYUVLuminanceSource(copy, preview_width, preview_height, 0, 0, preview_width, preview_height, false))));
								}
								catch (NotFoundException exception)
								{
									return;
								}
								catch (FormatException exception)
								{
									return;
								}
								catch (ChecksumException exception)
								{
									return;
								}
								haveCode = true;

								runOnUiThread(new Runnable()
								{
									@Override
									public void run()
									{
										// pause camera

										mCamera.stopPreview();

										// QR -> venue

										Venue venue = null;
										try
										{
											venue = Venue.fromCode(result.getText());
										}
										catch (Venue.ClientException exception)
										{
											// empty
										}
										catch (Venue.IncorrectCodeTypeException exception)
										{
											((TextView) findViewById(R.id.info)).setText(R.string.scan_error_unrecognised_format);
										}
										catch (Venue.UnsupportedVersionException exception)
										{
											((TextView) findViewById(R.id.info)).setText(R.string.scan_error_unsupported_version);
										}
										catch (Venue.MalformedCodeException exception)
										{
											((TextView) findViewById(R.id.info)).setText(R.string.scan_error_malformed_code);
										}
										catch (Venue.InvalidSignatureException exception)
										{
											((TextView) findViewById(R.id.info)).setText(R.string.scan_error_invalid_signature);
										}
										if (venue != null)
										{
											((ImageView) findViewById(R.id.icon)).setImageResource(R.drawable.code_accepted);
											((Animatable) ((ImageView) findViewById(R.id.icon)).getDrawable()).start();
											findViewById(R.id.icon).setVisibility(View.VISIBLE);

											((TextView) findViewById(R.id.info)).setText(venue.name);

											((Button) findViewById(R.id.negative)).setText(R.string.cancel);
											findViewById(R.id.negative).setOnClickListener(new View.OnClickListener()
											{
												@Override
												public void onClick(View view)
												{
													setResult(RESULT_CANCELED);
													finish();
												}
											});
											findViewById(R.id.negative).setVisibility(View.VISIBLE);
											((Button) findViewById(R.id.positive)).setText(R.string.confirm);
											final Venue finalVenue = venue;
											findViewById(R.id.positive).setOnClickListener(new View.OnClickListener()
											{
												@Override
												public void onClick(View view)
												{
													Intent data = new Intent();
													data.putExtra(EXTRA_VENUE, finalVenue);
													setResult(RESULT_OK, data);
													finish();
												}
											});
											findViewById(R.id.positive).setVisibility(View.VISIBLE);

											findViewById(R.id.cancel).setVisibility(View.GONE);
											findViewById(R.id.bottom).setVisibility(View.VISIBLE);
										}
										else
										{
											((ImageView) findViewById(R.id.icon)).setImageResource(R.drawable.code_rejected);
											((Animatable) ((ImageView) findViewById(R.id.icon)).getDrawable()).start();
											findViewById(R.id.icon).setVisibility(View.VISIBLE);

											((Button) findViewById(R.id.negative)).setText(R.string.close);
											findViewById(R.id.negative).setOnClickListener(new View.OnClickListener()
											{
												@Override
												public void onClick(View view)
												{
													setResult(RESULT_CANCELED);
													finish();
												}
											});
											findViewById(R.id.negative).setVisibility(View.VISIBLE);
											findViewById(R.id.positive).setVisibility(View.GONE);

											findViewById(R.id.cancel).setVisibility(View.GONE);
											findViewById(R.id.bottom).setVisibility(View.VISIBLE);
										}
									}
								});
							}
							finally
							{
								lock.release();
							}
						}
					}).start();
				}
			}
		});

		// start camera

		if (((TextureView) findViewById(R.id.preview)).isAvailable())
		{
			try
			{
				mCamera.setPreviewTexture(((TextureView) findViewById(R.id.preview)).getSurfaceTexture());
				setPreviewTransformation();
			}
			catch (IOException exception)
			{
				// empty
			}
		}
		mCamera.startPreview();
	}

	private void setPreviewTransformation()
	{
		Camera.Size camera_size = mCamera.getParameters().getPreviewSize();
		float camera_ratio = (float) camera_size.width / (float) camera_size.height;
		if (mCameraRotation == 90 || mCameraRotation == 270)
		{
			camera_ratio = 1.0f / camera_ratio;
		}

		TextureView view = findViewById(R.id.preview);
		int view_width = view.getWidth();
		int view_height = view.getHeight();
		float view_ratio = (float) view_width / (float) view_height;

		Matrix matrix = new Matrix();
		if (view_ratio > camera_ratio)
		{
			matrix.setScale(1.0f, view_ratio / camera_ratio, (float) view_width / 2.0f, (float) view_height / 2.0f);
		}
		else
		{
			matrix.setScale(camera_ratio / view_ratio, 1.0f, (float) view_width / 2.0f, (float) view_height / 2.0f);
		}
		if (mCameraMirrored)
		{
			matrix.postScale(-1.0f, 1.0f, (float) view_width / 2.0f, (float) view_height / 2.0f);
		}
		view.setTransform(matrix);
	}
}