package covidsecure.uk.venuecheckin;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

import covidsecure.uk.venuecheckin.database.AppDatabase;
import covidsecure.uk.venuecheckin.database.CheckInDao;
import covidsecure.uk.venuecheckin.database.CheckIn;

public class ClearOldHistoryWorker extends Worker
{
	public ClearOldHistoryWorker(@NonNull Context context, @NonNull WorkerParameters workerParams)
	{
		super(context, workerParams);
	}

	@NonNull
	@Override
	public Result doWork()
	{
		LocalDateTime expired = LocalDateTime.of(LocalDate.now().minusDays(21), LocalTime.MIDNIGHT);
		CheckInDao dao = AppDatabase.getInstance(getApplicationContext()).checkInDao();
		for (CheckIn checkIn : dao.getCheckInHistorySynchronously())
		{
			if (LocalDateTime.ofInstant(Instant.ofEpochSecond(checkIn.getStart()), ZoneId.systemDefault()).isBefore(expired))
			{
				dao.removeCheckIn(checkIn);
			}
		}
		return Result.success();
	}
}