package covidsecure.uk.venuecheckin;

import androidx.multidex.MultiDexApplication;

public class VenueCheckInApplication extends MultiDexApplication
{
	@Override
	public void onCreate()
	{
		super.onCreate();

		Notifications.createNotificationChannels(this);
		Workers.scheduleWork(this);
	}
}