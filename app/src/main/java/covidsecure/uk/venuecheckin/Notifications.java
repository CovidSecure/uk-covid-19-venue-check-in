package covidsecure.uk.venuecheckin;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.media.RingtoneManager;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class Notifications
{
	private static final String RISKY_VENUE_CHANNEL_ID = "risky_venue";
	private static final String STALE_DATA_CHANNEL_ID = "stale_data";
	private static int RISKY_VENUE_NOTIFICATION_ID = 1;
	private static int STALE_DATA_NOTIFICATION_ID = 2;

	public static void createNotificationChannels(Context context)
	{
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
		{
			String riskyVenueNotificationChannelName = context.getString(R.string.risky_venue_notification_channel_name);
			String riskyVenueNotificationChannelDescription = context.getString(R.string.risky_venue_notification_channel_description);
			int riskyVenueNotificationChannelImportance = NotificationManager.IMPORTANCE_DEFAULT;
			NotificationChannel riskyVenueNotificationChannel = new NotificationChannel(RISKY_VENUE_CHANNEL_ID, riskyVenueNotificationChannelName, riskyVenueNotificationChannelImportance);
			riskyVenueNotificationChannel.setDescription(riskyVenueNotificationChannelDescription);

			String staleDataNotificationChannelName = context.getString(R.string.stale_data_notification_channel_name);
			String staleDataNotificationChannelDescription = context.getString(R.string.stale_data_notification_channel_description);
			int staleDataNotificationChannelImportance = NotificationManager.IMPORTANCE_LOW;
			NotificationChannel staleDataNotificationChannel = new NotificationChannel(STALE_DATA_CHANNEL_ID, staleDataNotificationChannelName, staleDataNotificationChannelImportance);
			staleDataNotificationChannel.setDescription(staleDataNotificationChannelDescription);
			staleDataNotificationChannel.setShowBadge(false);

			NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
			notificationManager.createNotificationChannel(riskyVenueNotificationChannel);
			notificationManager.createNotificationChannel(staleDataNotificationChannel);
		}
	}

	public static void showRiskyVenueNotification(Context context)
	{
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context, RISKY_VENUE_CHANNEL_ID);
		builder.setSmallIcon(R.drawable.ic_venue_notification);
		builder.setContentTitle(context.getString(R.string.risky_venue_notification_title));
		builder.setStyle(new NotificationCompat.BigTextStyle().bigText(context.getString(R.string.risky_venue_notification_text)));
		builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
		builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
		builder.setShowWhen(false);
		builder.setOnlyAlertOnce(true);
		NotificationManagerCompat.from(context).notify(RISKY_VENUE_NOTIFICATION_ID, builder.build());
	}

	public static void showStaleDataNotification(Context context)
	{
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context, STALE_DATA_CHANNEL_ID);
		builder.setSmallIcon(R.drawable.ic_stale_data);
		builder.setContentTitle(context.getString(R.string.stale_data_notification_title));
		builder.setStyle(new NotificationCompat.BigTextStyle().bigText(context.getString(R.string.stale_data_notification_text)));
		builder.setPriority(NotificationCompat.PRIORITY_LOW);
		builder.setShowWhen(false);
		builder.setOnlyAlertOnce(true);
		NotificationManagerCompat.from(context).notify(STALE_DATA_NOTIFICATION_ID, builder.build());
	}

	public static void removeStaleDataNotification(Context context)
	{
		NotificationManagerCompat.from(context).cancel(STALE_DATA_NOTIFICATION_ID);
	}
}