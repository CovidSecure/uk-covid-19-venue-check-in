package covidsecure.uk.venuecheckin;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

public class StaleDataNotificationWorker extends Worker
{
	private static final String LAST_STALE_DATA_NOTIFICATION_TIME_PREFERENCE_KEY = "lastStaleDataNotificationTime";

	public StaleDataNotificationWorker(@NonNull Context context, @NonNull WorkerParameters workerParams)
	{
		super(context, workerParams);
	}

	@NonNull
	@Override
	public Result doWork()
	{
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		long lastUpdated = preferences.getLong(RiskyVenuesWorker.RISKY_VENUES_LAST_UPDATED_PREFERENCE_KEY, 0);
		if (lastUpdated != 0)
		{
			if (LocalDateTime.ofInstant(Instant.ofEpochSecond(lastUpdated), ZoneId.systemDefault()).isBefore(LocalDateTime.of(LocalDate.now().minusDays(1), LocalTime.MIDNIGHT)))
			{
				long lastNotified = preferences.getLong(LAST_STALE_DATA_NOTIFICATION_TIME_PREFERENCE_KEY, 0);
				if (LocalDateTime.ofInstant(Instant.ofEpochSecond(lastNotified), ZoneId.systemDefault()).isBefore(LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT)))
				{
					preferences.edit().putLong(LAST_STALE_DATA_NOTIFICATION_TIME_PREFERENCE_KEY, Instant.now().getEpochSecond()).apply();
					Notifications.showStaleDataNotification(getApplicationContext());
				}
			}
			else
			{
				Notifications.removeStaleDataNotification(getApplicationContext());
			}
		}
		return Result.success();
	}
}