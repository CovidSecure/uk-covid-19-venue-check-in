# UK COVID-19 Venue Check-in

Third-party app that allows users to check in to venues in the UK by scanning an NHS Test and Trace QR code if they don't want to or are unable to use the official NHS app. As with the official app, the user's check-in history is stored only on their device and is not shared with the NHS or anyone else. The app periodically downloads a list of venues with possible COVID-19 exposure and will alert the user if they have checked in to a venue that appears on this list during the relevant time period. History is automatically deleted after 21 days, and users can view and manually delete their history if desired.

## License

Copyright (C) 2020 CovidSecure

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
